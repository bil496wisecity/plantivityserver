from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from pprint import pprint
from time import sleep
from selenium.webdriver.chrome.options import Options
import json
import sys
from pprint import pprint
import requests
import urllib

API_KEY = "AIzaSyBGmF8TSASa_2keYmG4HxwZ-dHcFr5D5VE"
GEOCODE_API_URL = "https://maps.googleapis.com/maps/api/geocode/json?"

def wait_until_find_xpath(driver, xpath):
	t = True
	failCount = 0
	maxFail = 5

	while t and failCount < maxFail:
		try:
			element = driver.find_element_by_xpath(xpath)
			t = False
		except:
			t = True
			failCount = failCount + 1
			sleep(0.2)
	if (failCount == maxFail):
		return None
	else:
		return element



def openPage(driver, url):
	print(url)
	openingPageScript = '''window.open("{}");'''.format(url)
	driver.execute_script(openingPageScript)
	sleep(0.3)
	driver.switch_to_window(driver.window_handles[len(driver.window_handles) - 1])


def closePage(driver):
	driver.close()
	sleep(1)
	driver.switch_to_window(driver.window_handles[len(driver.window_handles) - 1])


def is_unappropiate(driver):
	return False # because of the COVID-19 every event is postponed.
	try:
		message = driver.find_element_by_xpath("//*[contains(@class, 'eventNosMessage_s04_canceled')]")
		print("Event is cancelled.")
		return True
	except:
		pass

	try:
		message = driver.find_element_by_xpath("//*[contains(@class, 'eventNosMessage_s02_soldout')]")
		print("Tickets are sold-out.")
		return True
	except:
		pass

	if wait_until_find_xpath(driver, "//*[@id='showList']"):
		print("This is not an event.")
		return True
	else:
		pass

	'''
	try:
		temp = driver.find_element_by_xpath("//*[@id='showList']") # sanity check
		print("This is not an event.")
		return True	
	except:
		pass
	'''

	return False


def get_image_url(driver):
	try:
		url = driver.find_element_by_xpath("//*[@class='eventGroupImage']/img").get_attribute('src')
		return url
	except:
		pass

	try:
		url = driver.find_element_by_xpath("//*[@id='ei_header']/div[2]/img").get_attribute('src')
		return url
	except:
		pass

	try:
		url = driver.find_element_by_xpath("//*[@id='ei_header']/div[3]/img").get_attribute('src')
		return url
	except:
		pass

	try:
		url = driver.find_element_by_xpath("//*[@id='ei_header']/div[4]/img").get_attribute('src')
		return url
	except:
		pass


import atexit

def dump_json(events_json):
	with open("./events.json", "w") as json_file:
		json.dump(events_json, json_file, indent=2)



def get_geocoded_event(event, API_KEY):
	r = requests.get('http://127.0.0.1:8000/cachedEventLocations/', json={'location_name': event['location']})
	if r.status_code == 200:
		event['lat'] = r.json()['lat']
		event['lng'] = r.json()['lng']
	else:
		params = {'address': event['location'], 'key': API_KEY}
		url_params = urllib.parse.urlencode(params)
		with urllib.request.urlopen(GEOCODE_API_URL + url_params) as response:
			geocode_response = json.load(response)
		location_json = geocode_response['results'][0]['geometry']['location']

		event['lat'] = location_json['lat']
		event['lng'] = location_json['lng']

		cached_event = {}
		cached_event['location_name'] = event['location']
		cached_event['lat'] = location_json['lat']
		cached_event['lng'] = location_json['lng']
		requests.post('http://127.0.0.1:8000/cachedEventLocations/', json=cached_event)
	return event

mainlink = "https://www.biletix.com"



if __name__ == '__main__':
	chrome_options = Options()  
	chrome_options.add_argument("--headless")  
	driver = webdriver.Chrome(chrome_options=chrome_options)
	driver.get("https://www.biletix.com/404-html/404.html")
	driver.set_page_load_timeout(120)

	infos = []


	main_genres = driver.find_elements_by_xpath("//*[@id='links']/ul")
	for i in range(len(main_genres)):
		main_genre_we = main_genres[i].find_element_by_xpath("./li[1]/a")
		blob = {}
		blob['main_genre'] = main_genre_we.text
		blob['sub_genres'] = []
		
		sub_genres = main_genres[i].find_elements_by_xpath("./li")
		for j in range(1, len(sub_genres)):
			sub_genre_we = sub_genres[j].find_element_by_xpath("./a")
			sub_genre_blob = {}
			sub_genre_blob['name'] = sub_genre_we.text
			sub_genre_blob['link'] = sub_genre_we.get_attribute('href')
			blob['sub_genres'].append(sub_genre_blob)
		infos.append(blob)
	
	events_json = []
	for blob in infos:
		for sub_genre in blob['sub_genres']:
			print(sub_genre)
			driver.get(sub_genre['link'])
			sleep(10)
			events = driver.find_elements_by_xpath("//*[@id='all_result']/div[4]/div")
			for i in range(len(events)):
				try:
					event_we = events[i].find_element_by_xpath("./div/div[1]")
					link = mainlink + event_we.get_attribute('onclick').split("\'")[1]
					print(link)
					if ("guides" in link) or ('etkinlik-grup' in link):
						continue
					#evading timeout exception
					try:
						openPage(driver, link)
						try:
							wait_until_find_xpath(driver, "//*[@id='_evidon-accept-button']").click()
						except:
							pass
						if is_unappropiate(driver):
							closePage(driver)
						else:
							event_blob = {}
							event_blob['main_genre'] = blob['main_genre']
							event_blob['sub_genre'] = sub_genre['name']
							event_blob['photo_url'] = get_image_url(driver)


							event_blob['event_name'] = driver.find_element_by_xpath("//*[@id='eventnameh1']/span").text
							try:
								event_blob['location'] = driver.find_element_by_xpath("//*[@itemprop='location']/a").text.replace("\n", "")
							except:
								print("INVALID LOCATION")
								event_blob['location'] = "aa"

							try:
								day = driver.find_element_by_xpath("//*[@class='e_daydate']").text
								month = driver.find_element_by_xpath("//*[@class='e_month']").text
								year = driver.find_element_by_xpath("//*[@class='e_year']").text
								weekday = driver.find_element_by_xpath("//*[@class='e_day']").text
								_time  = driver.find_element_by_xpath("//*[@class='e_time']").text
								event_blob['date'] = day + " " + month + " " + year + " " + weekday + " " + _time
							except:
								print("INVALID DATE.")
								event_blob['date'] = "aa"



							try:
								driver.find_element_by_xpath("//ul[@class='tabs']/li[2]/a").click()
								sleep(0.5)
								event_blob['price'] = driver.find_element_by_xpath("//*[@id='tab1']/div/div/div[2]/span[2]").text
								# event_blob['price'] = driver.find_element_by_xpath("//*[@itemprop='price']").text
								print(event_blob['price'])
							except:
								event_blob['price'] = None
								print("price exception")

							event_blob = get_geocoded_event(event_blob, API_KEY)
							r = requests.post('http://127.0.0.1:8000/event/', json=event_blob)
							events_json.append(event_blob)
							dump_json(events_json)
							sleep(2)
							closePage(driver)
					except:
						print("Timeout exception evaded.")
				except:
					print("Stale element reference evaded.")


# https://www.biletix.com/etkinlik/ZMK08/TURKIYE/tr
# invalid date.

# https://www.biletix.com/etkinlik/ZJ73X/TURKIYE/tr
# INVALID DATE.

# https://www.biletix.com/etkinlik/ZW310/TURKIYE/tr
# 10.00 TL

# https://www.biletix.com/etkinlik/ZAQ13/TURKIYE/tr
# 250.00 TL
# https://www.biletix.com/etkinlik/ZOU03/TURKIYE/tr
# 5.00 TL
