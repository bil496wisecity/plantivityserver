from selenium import webdriver
import time
from selenium.webdriver.support.ui import Select
import requests


def get_bonusses(bonus_web_element):
    a = bonus_web_element.find_elements_by_xpath('./li')
    bonusses = []
    for aa in a:
        # print(aa.text)
        bonusses.append(aa.text)
    return bonusses

if __name__ == "__main__":
    print("Hotel Web Scrapping Application Started ...")

    # chromedriver_path = r"C:\Users\BerkUtkuYenisey\Desktop\chromedriver_win32\chromedriver.exe"
    # initializing.
    driver_obj = webdriver.Chrome()
    hotels_website_url = "https://tr.hotels.com/"
    driver_obj.get(hotels_website_url)
    driver_obj.maximize_window()





    # search_button_element = driver_obj.find_element_by_xpath('//*[@id="hds-marquee"]/div[2]/div[1]/div/form/div[4]/button')
    # print("Printing type of search_button_element: ")
    # print(type(search_button_element))


    destination_location_element = driver_obj.find_element_by_id("qf-0q-destination")
    destination_location_element.send_keys("Ankara")

    time.sleep(2)
    destination_location_td_first = driver_obj.find_element_by_xpath("//tbody[@class='autosuggest-city']/tr[1]/td")
    destination_location_td_first.click()


    # Set check-in date.
    check_in_date_element = driver_obj.find_element_by_id("qf-0q-localised-check-in")
    check_in_date_element.clear()
    check_in_date_element.send_keys("21/04/2020")

    # Set check-out date.
    check_out_date_element = driver_obj.find_element_by_id("qf-0q-localised-check-out")
    check_out_date_element.clear()
    check_out_date_element.send_keys("23/04/2020")

    # close_date_window = driver_obj.find_elements_by_xpath('/html/body/div[7]/div[4]/button')
    # if we dont close that, search_button_element will be unclickable.
    time.sleep(2)
    close_date_window = driver_obj.find_element_by_xpath("//*[contains(@class, 'widget-daterange-control') and contains(@class, 'widget-overlay')]/button")
    close_date_window.click()

    # selecting adult count as 1.
    adult_count_dropdown = Select(driver_obj.find_element_by_id('qf-0q-room-0-adults'))
    adult_count_dropdown.select_by_value('1')



    search_button_element = driver_obj.find_element_by_xpath("//*[contains(@class, 'widget-query-group') and contains(@class, 'widget-query-ft')]/button")
    search_button_element.click()

    time.sleep(2)
    # Now we are in hotels screen.
    hotels = driver_obj.find_elements_by_xpath('//*[@id="listings"]/ol/li')

    print("\n")
    i = 1
    for hotel in hotels:
        hotel_json = {}
        try:
            hotel_json['name'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/h3/a').text
            hotel_json['address'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/address/span').text
            hotel_json['picture_url'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/figure/a').get_attribute('href')
            hotel_json['score'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/div/div[2]/strong').text
            hotel_json['reviews_url'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/div/div[2]/a').get_attribute('href')
            hotel_json['review_count'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/div/div[2]/a/span[1]').text
            try:
                hotel_json['price'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/aside/div[@class="price"]/a/strong').text
            except:
                # if there is an discount.
                hotel_json['price'] = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/aside/div[@class="price"]/a/ins').text

            try:
                bonus_web_element = hotel.find_element_by_xpath('//*[@id="listings"]/ol/li[' + str(i) + ']/article/section/div/div/div[1]/ul')
                hotel_json['bonusses'] = get_bonusses(bonus_web_element)
            except:
                pass
        except:
            pass

        print(hotel_json)
        r = requests.post('http://192.168.1.27:8000/hotels/', json=hotel_json)


        i = i + 1
        print('\n')

    # driver_obj.close()

    print("Hotel Web Scrapping Application Completed.")


# parkingOptions
# pool
# petFriendly
# airportTransfer
# gym
# aircondition







def try_until_find_it():
    i = 0
    while i < 5:
        try:
            search_button_element = driver_obj.find_element_by_xpath("//*[contains(@class, 'widget-query-group') and contains(@class, 'widget-query-ft')]/button")
            search_button_element.click()
            break
        except:
            time.sleep(0.5)
            i += 1