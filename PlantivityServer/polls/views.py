from django.shortcuts import render, redirect
from django.http import HttpResponse,JsonResponse
from pymongo import MongoClient
import json
from bson import Binary, Code, ObjectId
from bson.json_util import dumps
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import populartimes
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
from geopy.distance import geodesic
from bisect import bisect_left

import numpy as np
from sklearn import preprocessing
from sklearn.naive_bayes import GaussianNB
import io
from PIL import Image
import cv2
import imutils
from io import StringIO
import base64


client = MongoClient("mongodb+srv://Bil496:Test1@cluster0-8j5mo.mongodb.net/test?retryWrites=true&w=majority")
db = client.get_database('PlantivityDB')

API_KEY = "AIzaSyBGmF8TSASa_2keYmG4HxwZ-dHcFr5D5VE"
spotify_client_id = 'ac84cf15405b49fab3ba9addcafd385e'
spotify_client_secret = '4277947fa3c7409892a5936902141a6a'

def index(request):
	return render(request, 'index.html', {})

def room(request, room_name):
	return render(request, 'room.html', {
		'room_name': room_name
	})

class AllUsers(APIView):
	def get(self, request):
		users = db.USER
		data = json.loads(dumps(users.find()))
		return  JsonResponse(data, safe=False)
	def post(self, request):
		pass

class User(APIView):
	def get(self, request, email):
		users = db.USER
		data = json.loads(dumps(users.find({"email": email})))
		return  JsonResponse(data, safe=False)

	def post(self, request):
		users = db.USER
		data = request.data
		try:
			users.insert_one(data)
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_201_CREATED)

	def put(self, request, userEmail, deviceId):
		users = db.USER
		try:
			users.update_one({ "email": userEmail },{"$set":{ "deviceId": deviceId }})
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)

class Avatar(APIView):
	def put(self, request, userEmail):
		users = db.USER
		data = request.data
		try:
			users.update_one({ "email": userEmail },{"$set":{ "avatar": data['avatar'] }})
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)

class FriendFollowing(APIView):
	def put(self, request, selfEmail, userEmail):
		users = db.USER
		#print ("Name : ", user['email'])
		try:
			users.update({ "email": selfEmail },{ "$push": { "followings": userEmail } })
			users.update({ "email": userEmail },{ "$push": { "followers": selfEmail } })
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)

class FriendUnfollowing(APIView):
	def put(self, request, selfEmail, userEmail):
		users = db.USER
		#print ("Name : ", user['email'])
		try:
			users.update({ "email": selfEmail },{ "$pull": { "followings": userEmail } })
			users.update({ "email": userEmail },{ "$pull": { "followers": selfEmail } })
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)	

class AllRoutes(APIView):
	def get(self, request):
		routes = db.ROUTE
		data = json.loads(dumps(routes.find()))
		return  JsonResponse(data, safe=False)
	def post(self, request):
		pass

class Route(APIView):
	def get(self, request, userEmail):
		routes = db.ROUTE
		data = json.loads(dumps(routes.find({"userEmail": userEmail})))
		return  JsonResponse(data, safe=False)

	def post(self, request):
		routes = db.ROUTE
		data = request.data
		try:
			routes.insert(data)
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_201_CREATED)

	def delete(self, request, pk):
		routes = db.ROUTE
		try:
			routes.remove({ "_id": ObjectId(pk) })
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)

	def put(self, request, pk, rate):
		routes = db.ROUTE
		try:
			routes.update_one({ "_id": ObjectId(pk) },{"$set":{ "usersRate": float(rate) }})
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_202_ACCEPTED)
class Event(APIView):
	def get(self, request):
		events = db.EVENT
		events_json = json.loads(dumps(events.find()))
		return JsonResponse(events_json, safe=False)

	def post(self, request):
		events = db.EVENT
		data = request.data
		db_data = json.loads(dumps(events.find({"userId": data['event_name']})))
		if db_data != []:
			return Response({'Success': 'Event already exists on database.'}, status=status.HTTP_406_NOT_ACCEPTABLE)

		try:
			events.insert(data)
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_201_CREATED)


class NearbyEvents(APIView):
	def get(self, request, latlng, distance):
		lat = latlng.split(',')[0]
		lng = latlng.split(',')[1]

		current_lat = float(lat)
		current_lng = float(lng)

		events = db.EVENT
		events_json = json.loads(dumps(events.find()))
		nearby_events = []
		for event in events_json:
			# since we are using nosql, maybe data won't have any lat,lng data.
			try:
				event_lat = event['lat']
				event_lng = event['lng']
				latlng1 = (current_lat, current_lng)
				latlng2 = (event_lat, event_lng)
				calc = geodesic(latlng1, latlng2).meters
				if calc <= distance:
					nearby_events.append(event)
			except:
				pass

		return JsonResponse(nearby_events, safe=False)


class Populartimes(APIView):
	def get(self, request, place_id):
		response_json = populartimes.get_id(API_KEY, place_id)
		try:
			return JsonResponse(response_json['populartimes'], safe=False)
		except:
			jsonarray = []
			days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
			for day in days:
				block = {}
				block['name'] = day
				block['data'] = [0 for i in range(24)]
				jsonarray.append(block)
			return JsonResponse(jsonarray, safe=False)

class CachedEventLocations(APIView):
	def get(self, request, _json):
		cachedEventLocations = db.CACHED_EVENT_LOCATIONS
		cached_data = json.loads(dumps(cachedEventLocations.find({"location_name": _json['location_name']})))
		if cached_data == []:
			return Response({'Success': 'Location is not find on database.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
		else:
			return Response(cached_data[0], safe=False, status=status.HTTP_200_OK)

	def post(self, request):
		# I don't need to check if there is the same because I am only using post if get doesn't works.
		cachedEventLocations = db.CACHED_EVENT_LOCATIONS
		data = request.data
		try:
			cachedEventLocations.insert(data)
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_201_CREATED)


def reverse_insort(a, x, lo=0, hi=None):
	"""Insert item x in list a, and keep it reverse-sorted assuming a
	is reverse-sorted.

	If x is already in a, insert it to the right of the rightmost x.

	Optional args lo (default 0) and hi (default len(a)) bound the
	slice of a to be searched.
	"""
	if lo < 0:
		raise ValueError('lo must be non-negative')
	if hi is None:
		hi = len(a)
	while lo < hi:
		mid = (lo+hi)//2
		if x > a[mid]: hi = mid
		else: lo = mid+1
	a.insert(lo, x)


def insert(arr, keys, item, keyfunc=lambda v:v):
	if arr == []:
		arr.append(item)
	else:
		key = keyfunc(item)
		index = bisect_left(keys, key)
		keys.insert(index, key)
		arr.insert(index, item)


class VisitedPlaces(APIView):
	# get nearest 20 places with given lat, lng
	def get(self, request, latlng):
		visited_places = db.VISITED_PLACES
		visited_places_data = json.loads(dumps(visited_places.find()))

		lat = latlng.split(',')[0]
		lng = latlng.split(',')[1]
		current_lat = float(lat)
		current_lng = float(lng)

		nearest_twenty_place_indexes = []
		keys = []
		latlng1 = (current_lat, current_lng)
		for i in range(len(visited_places_data)):
			latlng2 = (visited_places_data[i]['lat'], visited_places_data[i]['lng'])
			distance = geodesic(latlng1, latlng2).meters
			_tuple = (i, distance)
			insert(nearest_twenty_place_indexes, keys, _tuple, keyfunc=lambda x:x[1])
		# nearest_twenty_place_indexes = nearest_twenty_place_indexes[:20]
		nearest_twenty_place = []
		for i in range(20):
			index = nearest_twenty_place_indexes[i][0]
			nearest_twenty_place.append(visited_places_data[index])

		return JsonResponse(nearest_twenty_place, safe=False)


	# Post means this place is visited, increase visited count if exists, if not add it to db.
	def post(self, request):
		visited_places = db.VISITED_PLACES
		data = request.data
		first_time_visit_count = 0
		updated_visit_count = 0
		for place in data['places']:
			check_data = json.loads(dumps(visited_places.find({"reference": place['reference']})))
			if check_data == []:
				insert_data = {}
				insert_data['name'] = place['name']
				insert_data['reference'] = place['reference']
				insert_data['user_ratings_total'] = place['user_ratings_total']
				insert_data['price_level'] = place['price_level']
				insert_data['lat'] = place['lat']
				insert_data['lng'] = place['lng']
				insert_data['rating'] = place['rating']
				insert_data['visit_count'] = 1
				visited_places.insert(insert_data)
				first_time_visit_count += 1
			else:
				visit_count_increased = check_data[0]['visit_count'] + 1
				visited_places.update_one({
					'reference': place['reference']
				}, {
					'$set': {
						'visit_count': visit_count_increased
					}
				}, upsert=False)
				updated_visit_count += 1

		return Response({'first_time_count': first_time_visit_count, 'updated_count': updated_visit_count}, status=status.HTTP_200_OK)



def get_place_relativity_score(best_person_genres, place):
	score = 0
	# try is a hack for handling missing fields, in this case ['music_genres']
	try:
		place_music_genres = place['music_genres']
		for i in range(len(best_person_genres)):
			if best_person_genres[i][0] in place_music_genres:
				score += 5 * best_person_genres[i][1]
		return score
	except:
		return score

def get_event_relativity_score(best_person_genres, place):
	score = 0
	try:
		place_music_genres = place['music_genres']
		for i in range(len(best_person_genres)):
			if best_person_genres[i][0] in place_music_genres:
				score += 5 * best_person_genres[i][1]
		return score
	except:
		return score



class SpotifyLinking(APIView):
	# example spotify_username: '11140647834'

	def post(self, request):
		data = request.data
		print("spotify stiitch {}".format(data))
		token = data['token']
		spotify_username = data['spotify_username']
		email = data['email']

		client_credentials_manager = SpotifyClientCredentials(client_id=spotify_client_id,
															  client_secret=spotify_client_secret)
		sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager, auth=token)

		# Filling user_artists_id first. Then this will used to retrieve artist data.
		user_artists_id = {}
		playlists = sp.user_playlists(spotify_username)
		while playlists:
			for i, playlist in enumerate(playlists['items']):
				playlist_extended = sp.playlist(playlist['id'])
				for track in playlist_extended['tracks']['items']:
					if track['track'] == None:
						continue
					artist_id = track['track']['artists'][0]['id']
					if artist_id not in user_artists_id:
						try:
							user_artists_id[artist_id] += 1
						except:
							user_artists_id[artist_id] = 1
						# user_artists_id.append(artist_id)
			if playlists['next']:
				playlists = sp.next(playlists)
			else:
				playlists = None

		# getting genres using artist ids.
		genres = {}
		for artist_id, artist_weight in user_artists_id.items():
			artist = sp.artist(artist_id)
			for genre in artist['genres']:
				try:
					genres[genre] += artist_weight
				except:
					genres[genre] = artist_weight

		genres = sorted(genres.items(), key=lambda item: item[1])
		genres.reverse()

		best_genres = []
		'''
		genre_count = 0
		max_genre_count = 5
		for genre_name, genre_weight in genres.items():
			genre_count += 1
			if genre_count == max_genre_count:
				break
			best_genres.append(genre_name)
		'''
		for i in range(5):
			best_genres.append(genres[i][0])

		'''
		genres = []
		for i in range(0, len(user_artists_id), 5):
			if len(user_artists_id) - 1 < i + 4:
				end = len(user_artists_id) - 1
			else:
				end = i + 4
			artists = sp.artists(user_artists_id[i:end])
			for artist in artists['artists']:
				for genre in artist['genres']:
					if genre not in genres:
						genres.append(genre)
		'''
		db.USER.update_one({
			'email': email
		}, {
			'$set': {
				'music_genres': genres
			}
		}, upsert=False)

		visited_places = db.VISITED_PLACES
		place_references = data['place_references']
		visited_places_data = json.loads(dumps(visited_places.find()))
		nearby_places = []
		for visited_place in visited_places_data:
			place_reference = visited_place['reference']
			if place_reference in place_references:
				place_relativity_score = get_place_relativity_score(best_genres, visited_place)
				block = {}
				block['score'] = place_relativity_score
				block['place_reference'] = visited_place['reference']
				nearby_places.append(block)


		sorted_nearby_places = sorted(nearby_places, key=lambda k: k['score'])
		sorted_nearby_places.reverse()
		most_related_nearby_places = sorted_nearby_places[:5]


		events = db.EVENTS
		event_titles = data['event_titles']
		event_data = json.loads(dumps(events.find()))
		nearby_events = []
		for event in event_data:
			event_name = event['event_name']
			if event_name in event_titles:
				event_relativity_score = get_place_relativity_score(best_genres, event)
				block = {}
				block['score'] = event_relativity_score
				block['event_title'] = event['event_name']
				nearby_events.append(block)

		sorted_nearby_events = sorted(nearby_events, key=lambda k: k['score'])
		sorted_nearby_events.reverse()
		most_related_nearby_events = sorted_nearby_events[:5]

		print("aaa")
		return Response({'events': most_related_nearby_events, 'places': most_related_nearby_places, 'music_genres': genres}, status=status.HTTP_200_OK)


class UpdatePlaceSpotify(APIView):
	def post(self, request):
		data = request.data
		visited_places = db.VISITED_PLACES
		events = db.EVENTS

		updated_places = data['updated_places']
		for i in range(len(updated_places)):
			print(updated_places[i])
			print(i)
			try:
				visited_places.update_one({
					'reference': updated_places[i]['reference']
				}, {
					'$set': {
						'music_genres': updated_places[i]['music_genres']
					}
				}, upsert=False)
			except:
				# this is an event but not handled.
				events.update_one({
					'event_name': updated_places[i]['event_name']
				}, {
					'$set': {
						'music_genres': updated_places[i]['music_genres']
					}
				}, upsert=False)


		updated_events = data['updated_events']
		for updated_event in updated_events:
			events.update_one({
				'event_name': updated_event['event_name']
			}, {
				'$set': {
					'music_genres': updated_event['music_genres']
				}
			}, upsert=False)
		return Response(status=status.HTTP_200_OK)



global clf_148
global minmax_scale

def train_popularity():
	visited_places = db.VISITED_PLACES

	# fetching places data which has popularity data.
	places_data = json.loads(dumps(visited_places.find()))
	features = []
	for i in range(len(places_data)):
		feature_block = []
		feature_block.append(places_data[i]['rating'])
		feature_block.append(places_data[i]['price_level'])
		feature_block.append(places_data[i]['user_ratings_total'])
		features.append(feature_block)
	features = np.array(features)

	minmax_scale = preprocessing.MinMaxScaler(feature_range=(0, 1))
	scaled_features = minmax_scale.fit_transform(features)

	results_148 = []
	for i in range(7):
		for j in range(24):
			hourly_popularity_array = []
			for k in range(len(places_data)):
				hourly_popularity_array.append(places_data[k]['popularity'][i]['data'][j])
			hourly_popularity_array = np.array(hourly_popularity_array)
			results_148.append(hourly_popularity_array)



	clf_148 = [GaussianNB() for i in range(148)]
	for i in range(7):
		for j in range(24):
			# Training data here. Training 148 times for every hour.
			index = i*24 + j
			clf_148[index].fit(scaled_features, results_148[index])









def predict_popularity():
	places_empty_popularity = db.PLACES_EMPTY_POPULARITY

	places_empty_data = json.loads(dumps(places_empty_popularity.find()))
	for i in range(7):
		for j in range(24):
			# Training data here. Training 148 times for every hour.
			index = i*24 + j
			for k in range(len(places_empty_data)):
				_features_to_be_predicted = []
				_features_to_be_predicted.append(places_empty_data[k]['rating'])
				_features_to_be_predicted.append(places_empty_data[k]['price_level'])
				_features_to_be_predicted.append(places_empty_data[k]['user_ratings_total'])
				scaled_features_to_be_predicted = minmax_scale.transform(_features_to_be_predicted)
				features_to_be_predicted = [scaled_features_to_be_predicted]
				predicted_value = clf_148[index].predict(features_to_be_predicted)[0]
				places_empty_data[k]['popularity'][i]['data'][j] = predicted_value


	# updating db, PLACES_EMPTY_POPULARITY -> PLACES_PREDICTED_POPULARITY
	places_predicted_popularity = db.PLACES_PREDICTED_POPULARITY
	for i in range(len(places_empty_data)):
		places_empty_popularity.delete_one({'reference': places_empty_data[i]['reference']})
		places_predicted_popularity.insert(places_empty_data[i])

	return places_empty_data


class Places(APIView):
	def get(self, request):
		visited_places_db = db.VISITED_PLACES
		events_json = json.loads(dumps(visited_places_db.find()))
		return JsonResponse(events_json, safe=False)

	def post(self, request):
		visited_places = db.VISITED_PLACES
		places_empty_popularity = db.PLACES_EMPTY_POPULARITY
		data = request.data
		places = data['places']
		success_data_found = 0
		empty_popularity_count = 0
		total_data = len(places)
		for place in places:
			check_data = json.loads(dumps(visited_places.find({"reference": place['reference']})))
			if check_data == []:
				check_data = json.loads(dumps(places_empty_popularity.find({"reference": place['reference']})))

			if check_data == []:
				success_data_found += 1
				db_obj = {}
				db_obj['groups'] = place['groups']
				db_obj['rating'] = place['rating']
				db_obj['price_level'] = place['price_level']
				db_obj['user_ratings_total'] = place['user_ratings_total']
				db_obj['reference'] = place['reference']
				db_obj['popularity'] = place['popularity']
				# db_obj['is_blank'] = place['is_blank']

				# "WRG9+H8 Emek, Çankaya/Ankara, Turkey"
				# "WRG7+HQ Ankara, Turkey"
				# "WRFF+V5 Yukarı Bahçelievler, Çankaya/Ankara, Turkey"
				# "WRJF+68 Bahçelievler, Çankaya/Ankara, Turkey"
				# "WRFG+RF Yukarı Bahçelievler, Çankaya/Ankara, Turkey"
				compound_code_items = place['compound_code'].split(",")[:-1]

				# "WRG9+H8 Emek, Çankaya/Ankara"
				# "WRG7+HQ Ankara"
				# "WRFF+V5 Yukarı Bahçelievler, Çankaya/Ankara"
				# "WRJF+68 Bahçelievler, Çankaya/Ankara"
				# "WRFG+RF Yukarı Bahçelievler, Çankaya/Ankara"

				# print("compound code: {}".format(place['compound_code']))
				if len(compound_code_items) == 2:
					place_small = " ".join(compound_code_items[0].split(" ")[1:])
					try:
						place_area = compound_code_items[1].split("/")[0].strip()
						place_city = compound_code_items[1].split("/")[1]
					except:
						place_area = "NONE"
						place_city = compound_code_items[1].strip()
				if len(compound_code_items) == 1:
					place_small = "NONE"
					place_area = "NONE"
					place_city = " ".join(compound_code_items[0].split(" ")[1:])

				# print("compound_code: {}".format(place['compound_code']))
				# print("id: {}".format(place['id']))
				db_obj['place_small'] = place_small
				db_obj['place_area'] = place_area
				db_obj['place_city'] = place_city

				if place['is_blank']:
					places_empty_popularity.insert(db_obj)
					empty_popularity_count += 1
				else:
					visited_places.insert(db_obj)

		if empty_popularity_count > 0:
			places_predicted_data = predict_popularity()
		return Response({'Success': '{} of {} is added to db. Predicted count is {}'.format(total_data, success_data_found, empty_popularity_count),
						 'predicted_data': places_predicted_data}, status=status.HTTP_200_OK)
						  
class Hotels(APIView):
	def get(self, request):
		hotels_db = db.HOTELS
		events_json = json.loads(dumps(hotels_db.find()))
		return JsonResponse(events_json, safe=False)

	def post(self, request):
		hotels_db = db.HOTELS
		data = request.data
		try:
			hotels_db.insert(data)
		except Exception:
			return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
		return Response(status=status.HTTP_201_CREATED)

'''
def byte_array_to_pil_image(byte_array):
	# byte_array = bytearray(byte_array)
	pil_image = Image.open(io.BytesIO(byte_array))
	return pil_image
'''

def pil_image_to_opencv_image(pil_image):
	opencv_image = cv2.cvtColor(np.array(pil_image), cv2.COLOR_RGB2BGR)
	opencv_image = cv2.cvtColor(opencv_image, cv2.COLOR_BGR2RGB)
	return opencv_image


def base64_to_pil_image(base64_img):
	decoded_base64 = base64.b64decode(base64_img)
	# img_string = StringIO(base64_img)
	f = io.BytesIO(decoded_base64)
	pil_img = Image.open(f)
	return pil_img


def opencv_image_to_pil_image(opencv_image):
	im_pil = Image.fromarray(opencv_image)
	# For reversing the operation:
	im_np = np.asarray(im_pil)
	return im_pil


def pil_image_to_base64(pil_image):
	buffered = io.BytesIO()
	pil_image.save(buffered, format="JPEG")
	img_str = base64.b64encode(buffered.getvalue())
	return img_str


def stitch_images(images):
	print("[INFO] stitching images...")
	stitcher = cv2.createStitcher() if imutils.is_cv3() else cv2.Stitcher_create()
	(status, stitched) = stitcher.stitch(images)

	# if the status is '0', then OpenCV successfully performed image
	if status == 0:
		pass
		# cv2.imshow("Stitched", stitched)
		# cv2.waitKey(0)
	# otherwise the stitching failed, likely due to not enough keypoints
	else:
		print("[INFO] image stitching failed ({})".format(status))
	return stitched


class Stitch(APIView):
	# if post, it means that user send photos_byte_array and requests for stitched_byte_array
	def post(self, request):
		data = request.data
		# print("request {}".format(request))
		# print("sttich data {}".format(data))

		images = []
		pictures_base64_array = data['pictures_byte_array']
		for picture_base64_array in pictures_base64_array:
			# pil_image = byte_array_to_pil_image(pictures_byte_array)
			pil_image = base64_to_pil_image(picture_base64_array)
			opencv_image = pil_image_to_opencv_image(pil_image)
			images.append(opencv_image)


		stitched_image = stitch_images(images)
		stitched_pil_image = opencv_image_to_pil_image(stitched_image)
		stitched_base64 = pil_image_to_base64(stitched_pil_image)


		# ducktaping for demo phone error.
		'''
		places_db = db.VISITED_PLACES
		reference = 'ChIJKVtTAyRP0xQROVMBpokyhwQ'
		places_json = json.loads(dumps(places_db.find({"reference": reference})))
		place = places_json[0]
		stitched_base64 = places_json[0]['panaromas'][0]
		stitched_pil_image = base64_to_pil_image(stitched_base64)
		stitched_opencv_image = cv2.cvtColor(np.array(stitched_pil_image), cv2.COLOR_BGR2RGB)
		returned_pil_image = opencv_image_to_pil_image(stitched_opencv_image)
		stitched_base64 = pil_image_to_base64(returned_pil_image)
		'''
		# print(stitched_base64)

		return Response({'byte_array_string': stitched_base64}, status=status.HTTP_200_OK)




class Panaroma(APIView):
	# if get, return user all panaromas with given reference.
	def get(self, request, reference):
		visited_places = db.VISITED_PLACES
		check_data = json.loads(dumps(visited_places.find({"reference": reference})))
		panaromas = []
		if check_data == []:
			# TODO handle if reference is not found.
			return Response({'message': 'no panaroma photo for this place.', 'panaromas': panaromas}, status=status.HTTP_200_OK)

		place = check_data[0]
		for panaroma in place['panaromas']:
			panaromas.append(panaroma)
		return Response({'panaromas': panaromas}, status=status.HTTP_200_OK)




	# if post, it means that user approved stitched image and sending it as byte_array.
	def post(self,request):
		visited_places = db.VISITED_PLACES
		data = request.data
		print(data['stitched_byte_array'])
		stitched_byte_array = data['stitched_byte_array']
		place_reference = data['reference']
		print("place reference {}".format(place_reference))
		# place_reference = "ChIJJ0q5cM5I0xQRuYO0iMoxwo8"

		check_data = json.loads(dumps(visited_places.find({"reference": place_reference})))
		if check_data == []:
			place_obj = {}
			place_obj['reference'] = place_reference
			place_obj['panaromas'] = [stitched_byte_array]
			visited_places.insert(place_obj)
		else:
			place = check_data[0]
			try:
				panaromas = place['panaromas']
				panaromas.append(stitched_byte_array)
			except:
				panaromas = [stitched_byte_array]
			visited_places.update_one({
				'reference': place_reference
			}, {
				'$set': {
					'panaromas': panaromas
				}
			}, upsert=False)
		return Response(status=status.HTTP_200_OK)





