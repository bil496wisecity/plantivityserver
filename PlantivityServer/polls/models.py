#from django.db import models
from djongo import models

# Create your models here.
class USER(models.Model):
    name = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    gender = models.CharField(max_length=10)

class PLACE(models.Model):
    name = models.CharField(max_length=50)

class EVENT(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField()
    date = models.DateField()
    lat = models.FloatField()
    lng = models.FloatField()
    sub_genre = models.CharField(max_length=50)
    main_genre = models.CharField(max_length=50)
    photo_url = models.CharField(max_length=50)