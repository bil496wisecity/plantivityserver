"""PlantivityServer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from polls import views
from django.contrib import admin
from django.urls import path
#from polls.views import *
from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('chat/', views.index, name='index'),
    path('chat/<str:room_name>/', views.room, name='room'),
    url(r'^allusers$', views.AllUsers.as_view()),
    path('user/<str:email>', views.User.as_view()),
    path('user/<str:userEmail>/<str:deviceId>', views.User.as_view()),
    path('avatar/<str:userEmail>', views.Avatar.as_view()),
    url(r'^user$', views.User.as_view()),
    url(r'^allroutes$', views.AllRoutes.as_view()),
    path('route/<str:userEmail>', views.Route.as_view()),
    path('route/<str:pk>/<str:rate>/', views.Route.as_view()),
    path('deleteroute/<str:pk>', views.Route.as_view()),
    url(r'^route$', views.Route.as_view()),
    path('friendfollow/<str:selfEmail>/<str:userEmail>/', views.FriendFollowing.as_view()),
    path('friendunfollow/<str:selfEmail>/<str:userEmail>/', views.FriendUnfollowing.as_view()),
    path('nearbyevents/<str:latlng>/<int:distance>', views.NearbyEvents.as_view()),
    path('event/', views.Event.as_view()),
    path('populartimes/<slug:place_id>', views.Populartimes().as_view()),
    path('cachedEventLocations/', views.CachedEventLocations().as_view()),
    path('spotifyLinking/', views.SpotifyLinking().as_view()),
    path('visitedPlaces/<str:latlng>', views.VisitedPlaces().as_view()),
    path('visitedPlaces/', views.VisitedPlaces().as_view()),
    path('places/', views.Places.as_view()),
    path('stitch/', views.Stitch.as_view()),
    path('panaroma/', views.Panaroma.as_view()),
    path('panaroma/<str:reference>', views.Panaroma.as_view()),
    path('hotels/', views.Hotels().as_view()),
    path('updatePlaceSpotify/', views.UpdatePlaceSpotify().as_view())
]
urlpatterns = format_suffix_patterns(urlpatterns)
